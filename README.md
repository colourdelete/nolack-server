# Nolack Server

:warning: `TODO`: Support `imap_tools` 0.17.0 and over.

## Install

:information_source: It is good pratice to do this in a VM or any other complete sandboxed debian-based environment.

### From `sudo`er

:warning: Make sure to check any scripts running on a machne before execution.

:warning: `sudo` priveledges needed.

```
curl https://gitlab.com/colourdelete/nolack-server/-/raw/master/install.sh | sudo bash
```

### From `root`

:warning: Make sure to check any scripts running on a machne before execution.

:warning: Need to be `root`.

```
curl https://gitlab.com/colourdelete/nolack-server/-/raw/master/install.sh | bash
```

## Run

### From `sudo`er

:warning: Make sure to check any scripts running on a machne before execution.

:warning: `sudo` priveledges needed.

1. `sudo`, `curl`, `systemctl`などが入っていて、動いていることを確認してください。
2. `curl https://gitlab.com/colourdelete/nolack-server/-/raw/master/install.sh | sudo bash`を実行してください。
3. `/opt/nolack/nolack-server/config/`に、`service-account.json`と`gmail-credentails.json`と`stop_order.txt`をおいて下さい。
4. `/opt/nolack/nolack-server/run/autoboot.sh`を実行してください。


## Todo

- [ ] LINE Stuff
- [ ] Numerous Bugs!
- [ ] Caching to prevent Quota exceeding
