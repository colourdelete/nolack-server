from . import main # Main Program
# from . import install # Installer (systemd, etc)
# from . import mail # Mail Wrapper (Gmail?)
# from . import line # LINE Wrapper
from . import firestore # Firestore

__version__ = '0.0.0'
__channel__ = 'alpha' # [alpha] beta gamma delta epsilon
