import smtplib
from imap_tools import MailBox, Q
import traceback
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import datetime
from jinja2 import *
from . import log
logger = log.logger


class conn():
  def __init__(self, smtp_server, imap_server, username):
    self.smtp_server = smtp_server
    self.imap_server = imap_server
    self.username = username

  def send(
      self,
      from_,
      to,
      cc=None,
      bcc=None,
      subject='',
      body='',
  ):
    assert isinstance(to, str) or isinstance(to, list)
    print('Warning: cc, bcc is IGNORED!')
    msg = MIMEMultipart()
    msg['From'] = from_
    msg['To'] = to
    msg['Subject'] = subject
    msg.attach(MIMEText(body, 'html'))
    to = to if isinstance(to, list) else [to]
    email = f'From: {from_}\nTo: {to}\nSubject: {subject}\n\n{body}'
    # self.server.sendmail(from_, to, email, )
    self.smtp_server.sendmail(
        from_,
        to,
        msg.as_string(),
    )

  def get_all(self, ):
    messages = [msg for msg in self.imap_server.fetch(Q(all=True))]
    return messages


class host():
  def __init__(
      self,
      smtp_host='smtp.gmail.com',
      imap_host='imap.gmail.com',
      smtp_port=465,
  ):
    assert isinstance(smtp_host, str)
    assert isinstance(imap_host, str)
    assert isinstance(smtp_port, int) or isinstance(smtp_port, str)
    smtp_port = int(smtp_port)
    self.smtp_host = smtp_host
    self.imap_host = imap_host
    self.smtp_port = smtp_port
    self.server = None
    self.mailbox = None

  def connect(
      self,
      username,
      password,
      mailbox_name='Inbox',
      ssl=True,
      use_conn=True,
  ):
    smtp_server = self.connect_smtp(
        username,
        password,
        ssl=ssl,
        use_conn=False,
    )
    imap_server = self.connect_imap(
        username,
        password,
        mailbox_name=mailbox_name,
        ssl=ssl,
        use_conn=False,
    )
    if use_conn:
      result = conn(smtp_server, imap_server, username)
    else:
      result = smtp_server, imap_server, username
    return result

  def connect_smtp(
      self,
      username,
      password,
      ssl=True,
      use_conn=True,
  ):
    assert isinstance(ssl, bool)
    assert isinstance(use_conn, bool)
    if ssl:
      server = smtplib.SMTP_SSL(
          self.smtp_host,
          self.smtp_port,
      ) # port 465 for ssl
    elif ssl == False:
      server = smtplib.SMTP(
          self.smtp_host,
          self.smtp_port,
      ) # port 587 for non-ssl
    server.login(username, password)
    if use_conn:
      result = conn(server, None, username)
    else:
      result = server
    self.server = server
    return result

  def connect_imap(
      self,
      username,
      password,
      mailbox_name='Inbox',
      ssl=True,
      use_conn=True,
  ):
    mailbox = MailBox(self.imap_host, )
    mailbox.login(username, password, initial_folder=mailbox_name)
    if use_conn:
      result = conn(None, mailbox, username)
    else:
      result = mailbox
    return result

  def close(self, ):
    self.server.close()
    self.mailbox.logout()


class daemon():
  def __init__(
      self,
      config,
      gmail_credentials,
      database,
      ci_commit_sha='unknown',
      smtp_host=None,
      imap_host=None,
      smtp_port=None,
  ):
    logger.info('Mail Daemon: Starting...')
    self.config = config
    self.database = database
    self.ci_commit_sha = ci_commit_sha
    if smtp_host == None:
      self.smtp_host = config['mail_config']['smtp_host']
    else:
      self.smtp_host = smtp_host
    if imap_host == None:
      self.imap_host = config['mail_config']['imap_host']
    else:
      self.imap_host = imap_host
    if smtp_port == None:
      self.smtp_port = config['mail_config']['smtp_port']
    else:
      self.smtp_port = smtp_port
    logger.info('Mail Daemon: Accessing Mail Server...')
    self.server = host(
        smtp_host=self.smtp_host,
        imap_host=self.imap_host,
        smtp_port=self.smtp_port,
    )
    self.conn = self.server.connect(
        gmail_credentials['username'],
        gmail_credentials['password'],
    )
    self.from_ = gmail_credentials['username']
    logger.info('Mail Daemon: Testing...')
    logger.debug('Mail Daemon: (no tests)')
    logger.info('Mail Daemon: Testing Done.')

  def send_mail(
      self,
      _subscription,
      message,
      _demands,
  ):
    logger.debug('Mail Daemon: Send Mail: Email: ')
    logger.debug(_subscription.to_dict()['email'])
    logger.debug('Mail Daemon: Send Mail: Name:  ')
    logger.debug(_subscription.to_dict()['name'])
    logger.debug('Mail Daemon: Send Mail: Preparing email.')
    logger.debug('Mail Daemon: Send Mail: Parse subscription, demands...')
    subscription = _subscription.to_dict()
    subscription_id = _subscription.id
    demands = []
    for _demand in _demands:
      demands.append(_demand.to_dict())
    logger.debug('Mail Daemon: Send Mail: Parsed subscription, demands.')
    confirm_sent = self.database.get_subscription_confirm_sent(
        subscription_id, )
    logger.debug('Mail Daemon: Send Mail: Checked confirmSent. Sent.')
    if confirm_sent:
      logger.debug('Mail Daemon: Send Mail: Checking confirmSent...')
      logger.debug('Mail Daemon: Send Mail: Checking confirmOkByUser...')
      confirm_ok_by_user = self.database.get_subscription_confirm_ok_by_user(
          subscription_id, )
      if confirm_ok_by_user:
        logger.info('Mail Daemon: Send Mail: Checked confirmOkByUser. OK.')
      else:
        logger.info(
            'Mail Daemon: Send Mail: Checked confirmOkByUser. Not confirmed.')
        return 2, None, None
      logger.debug('Mail Daemon: Send Mail: Checking lastSent...')
      last_sent = self.database.get_subscription_last_sent(subscription_id, )
      send_time = last_sent + datetime.timedelta(
          days=int(subscription['frequency']))
      now_time = self.database.get_now()
      if send_time <= now_time:
        logger.info('Mail Daemon: Send Mail: Checked lastSent. OK.')
      else:
        logger.info('Mail Daemon: Send Mail: Checked lastSent. Too soon.')
        return 2, None, None
      mail_type = 'newsletter'
    else:
      logger.debug('Mail Daemon: Send Mail: Checked confirmSent. Not Sent.')
      logger.debug('Mail Daemon: Send Mail: Not Checking confirmOkByUser.')
      logger.debug('Mail Daemon: Send Mail: Not Checking lastSent.')
      logger.debug('Mail Daemon: Send Mail: Setting confirmSent. To Sent...')
      self.database.set_subscription_confirm_sent(
          subscription_id,
          True,
      )
      logger.debug('Mail Daemon: Send Mail: Setted confirmSent. To Sent.')
      mail_type = 'confirm'
    logger.debug(
        'Mail Daemon: Send Mail: Getting To, CC, BCC, Now Date, Vars...')
    to = subscription['email']
    from_ = self.from_
    cc = ''
    bcc = ''
    now_date = datetime.datetime.today()
    subscription_name = ''
    for name in subscription['name']:
      subscription_name += name
      subscription_name += ' '
    variables = {
        'now_date': now_date, # Today's date!
        'now_date_fmted': now_date.strftime('%Y-%m-%d'), # Today's date!
        'ci_commit_sha': self.ci_commit_sha,
        'msg': message, # Message from team
        'demands': demands, # List of demands
        'subscription': subscription,
        'subscription_id': subscription_id,
        'name': subscription_name,
    }
    logger.debug('Mail Daemon: Send Mail: Getted To, CC, BCC, Now Date, Vars.'
                 ) # Typo is on purpose.
    logger.debug('Mail Daemon: Send Mail: Template Engine: Starting...')
    env = Environment(loader=FileSystemLoader(
        searchpath=self.config['mail_config']['template_search_path']),
                      autoescape=select_autoescape(['html', 'xml']))
    logger.debug('Mail Daemon: Send Mail: Template Engine: Started.')
    logger.debug(
        'Mail Daemon: Send Mail: Template Engine: Rendering Subject...')
    if mail_type == 'newsletter':
      subject_template = env.get_template(
          self.config['mail_config']['subject_template'])
    elif mail_type == 'confirm':
      subject_template = env.get_template(
          self.config['mail_config']['confirm_subject_template'])
    subject = subject_template.render(**variables)
    logger.debug('Mail Daemon: Send Mail: Template Engine: Rendered Subject.')
    logger.debug('Mail Daemon: Send Mail: Template Engine: Rendering Body...')
    if mail_type == 'newsletter':
      body_template = env.get_template(
          self.config['mail_config']['body_template'])
    elif mail_type == 'confirm':
      body_template = env.get_template(
          self.config['mail_config']['confirm_body_template'])
    body = body_template.render(**variables)
    logger.debug('Mail Daemon: Send Mail: Template Engine: Rendered Body.')
    logger.debug('Mail Daemon: Send Mail: Prepared email.')
    logger.debug('Mail Daemon: Send Mail: * means ignored (as of now)')
    logger.debug(f'Mail Daemon: Send Mail: Subject: {subject}')
    logger.debug(f'Mail Daemon: Send Mail: From:    {from_}')
    logger.debug(f'Mail Daemon: Send Mail: To:     {to}')
    logger.debug(f'Mail Daemon: Send Mail: CC*:     {cc}')
    logger.debug(f'Mail Daemon: Send Mail: BCC*:    {bcc}')
    logger.debug(f'Mail Daemon: Send Mail: Body:    {body}')
    try:
      self.conn.send(
          from_,
          to,
          cc,
          bcc,
          subject,
          body,
      )
    except Exception as err:
      logger.error(f'Mail Daemon: Send Mail: Failed: {str(err)}')
      return 1, err, str(traceback.format_exc())
    else:
      logger.info(f'Mail Daemon: Send Mail: Success')
      logger.info(f'Mail Daemon: Send Mail: Setting lastSent to now...')
      self.database.set_subscription_last_sent(subscription_id,
                                               datetime.datetime.utcnow())
      logger.info(f'Mail Daemon: Send Mail: Setted lastSent to now.'
                  ) # Typo is on purpose.
      return 0, None, None

  def single_loop_command(
      self,
      ignore=None,
  ):
    print('Mail Daemon: Single Loop (Command): Starting...')
    print('Mail Daemon: Single Loop (Command): Started.')
    print('Mail Daemon: Single Loop (Command): Getting commands...')
    commands = self.database.get_applicable_commands(id_=self.config['id'],
                                                     name=self.config['name'])
    print('Mail Daemon: Single Loop (Command): Getted commands.'
          ) # Typo is on purpose.
    print('Mail Daemon: Single Loop (Command): Looping commands...')
    for command in commands:
      if command['module'] != 'mail':
        print(
            'Mail Daemon: Single Loop (Command): Loop: Skip (non-responsible).'
        )
        continue
      if command['command'] not in ('send'):
        print(
            'Mail Daemon: Single Loop (Command): Loop: Skip (invalid command).'
        )
        continue
    print('Mail Daemon: Single Loop (Command): Looped commands.')
    print('Mail Daemon: Single Loop (Command): Done.')

  def single_loop_mail(
      self,
      ignore=None,
  ):
    print('Mail Daemon: Single Loop (Mail): Starting...')
    print('Mail Daemon: Single Loop (Mail): Started.')
    print(
        'Mail Daemon: Single Loop (Mail): Getting demands, subscriptions, message...'
    )
    demands = self.database.get_demands()
    subscriptions = self.database.get_subscriptions('mail')
    message = self.database.get_applicable_message('mail')
    print(
        'Mail Daemon: Single Loop (Mail): Getted demands, subscriptions, message.'
    )
    for subscription in subscriptions:
      try:
        self.send_mail(
            subscription,
            message,
            demands,
        )
        print(f'Mail Daemon: Single Loop (Mail): Loop: Sent Mail.')
      except Exception as err:
        tb = traceback.format_exc()
        print(
            f'Mail Daemon: Single Loop (Mail): Loop: Error ({str(type(err))}, {str(err)}).'
        )
        print(tb)
