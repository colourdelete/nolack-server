import traceback
import flask
import datetime
from jinja2 import *
import time
import json
import yaml
import time
import multiprocessing
import threading
from . import main # Main Program
from . import mail # Mail Wrapper (usuallt Gmail)
# from . import line # LINE Wrapper
from . import firestore # Firestore
from . import log
logger = log.logger

import time
import multiprocessing


def loop(config, mail_daemon):
  logger.info('Main: Loop: Starting...')
  loop_length = config['main_config']['loop_length']
  exit = check_stop_order(config)
  logger.info('Main: Loop: Started.')
  while not exit:
    start_time = time.time()

    logger.debug('Main: Loop: Calling Single Loop...')
    try:
      single_loop_thread = threading.Thread(target=single_loop,
                                            args=[mail_daemon])
      single_loop_thread.start()
      single_loop_thread.join()
    except Exception as err:
      tb = traceback.format_exc()
      logger.warning(f'Main: Loop: Error ({str(type(err))}, {str(err)}).')
      logger.warning('Main: Loop: Error: Traceback:')
      logger.warning(tb)
    else:
      logger.debug('Main: Loop: Called Single Loop.')
    while time.time() < start_time + loop_length:
      time.sleep(config['main_config']['loop_wait_time'])
  logger.debug('Main: Loop: Done.')
  if exit:
    logger.error('Main: Loop: Exit w/ code 0!')
    exit(0)


def check_stop_order(config):
  logger.debug('Main: Loop: Checking if stop order...')
  try:
    with open(config['main_config']['stop_order_file'], 'r') as file:
      content = file.read().split(' ')
      if 'stop' in content[0]:
        logger.error('Main: Loop: Checked if stop order: STOP!')
        logger.error('Main: Loop: STOP PROGRAM!')
        logger.error('Main: Loop: Exit w/ code 0!')
        return True
  except Exception as err:
    logger.warning(
        f'Main: Loop: Checked if stop order: ERROR when acessing ({str(type(err))}, {str(err)}. So, continue.'
    )
    return False
  else:
    logger.debug('Main: Loop: Checked if stop order: continue')
    return False


def single_loop(mail_daemon):
  # pool = multiprocessing.Pool()
  # pool.map(mail_daemon.single_loop_mail, [None])
  # pool.map(mail_daemon.single_loop_command, [None])
  # pool.close()
  mail_daemon.single_loop_mail()
  mail_daemon.single_loop_command()


def run(
    *args,
    **kwargs,
):
  logger.info('Main: Starting...')
  ci_commit_sha = '<error while fetching ci_commit_sha>'
  try:
    with open('.info/ci_commit_sha.txt') as file:
      ci_commit_sha = file.read()
  except OSError as err:
    logger.warning('Unable to fetch ci_commit_sha.')
  logger.info('Main: Started.')
  logger.info('')
  logger.info('Nolack Server')
  logger.info(f'Version {ci_commit_sha}')

  logger.debug('Called with arguments:')
  logger.debug('Args:')
  for value in args:
    logger.debug(f'  {value}')
  logger.debug('Kwargs:')
  for key, value in kwargs.items():
    logger.debug(f'  {key} = {value}')
  logger.debug('')

  logger.debug('Main: Readying...')
  __version__ = ci_commit_sha
  __channel__ = 'alpha' # [alpha] beta gamma delta epsilon
  logger.debug('Main: Readied.')

  logger.info('Main: Getting Config...')
  logger.debug('Main: Trying .yaml...')
  try:
    with open('/opt/nolack/nolack-server/config/config.yaml', 'r') as file:
      config = yaml.safe_load(file)
  except FileNotFoundError as err:
    logger.debug('Main: Get Config: Exception occurred.')
    logger.debug('Main: Get Config: Trying .yml...')
    try:
      with open('/opt/nolack/nolack-server/config/config.yml', 'r') as file:
        config = yaml.safe_load(file)
    except FileNotFoundError as err:
      logger.debug('Main: Get Config: Exception occurred.')
      logger.debug('Main: Get Config: Trying .json...')
      try:
        with open('/opt/nolack/nolack-server/config/config.yaml', 'r') as file:
          config = json.load(file)
      except FileNotFoundError as err:
        tb = traceback.format_exc() # TODO: Add logging support
        logger.debug(tb)
        logger.debug('Main: Get Config: Using hardcorded default...')
        config = {
            "id": None,
            "name": "default",
            "main_config": {
                "mode":
                "loop",
                "loop_length":
                10,
                "loop_wait_time":
                0.1,
                "stop_order_file":
                "/opt/nolack/nolack-server/config/stop_order.txt"
            },
            "firestore": True,
            "line": False,
            "mail": True,
            "mail_config": {
                "interval": 7,
                "host": "smtp.gmail.com",
                "port": 465,
                "template_search_path":
                "/opt/nolack/nolack-server/<ci_commit_sha>/nolack-server/nolack_server/templates/",
                "subject_template": "email/subject.html",
                "body_template": "email/body.html"
            }
        }
      else:
        logger.info('Main: Getted Config. (.json)')
    else:
      logger.info('Main: Getted Config. (.yml)')
  else:
    logger.info('Main: Getted Config. (.yaml)')

  logger.info('Main: Getting Gmail Credentials...')
  try:
    with open('/opt/nolack/nolack-server/config/gmail-credentials.json',
              'r') as file:
      gmail_credentials = json.load(file)
  except Exception as err:
    logger.error('Main: Get Gmail credentials: Error:')
    tb = traceback.format_exc() # TODO: Add logging support
    logger.error(tb)
    logger.error('Main: Exiting w/ code 1...')
    exit(1)
  else:
    logger.info('Main: Getted Gmail Credentials.')

  logger.info('Accessing Firestore...') # TODO: Add support for `config.json`.
  database = firestore.firestore_wrapper(config)
  logger.info('Testing...')
  logger.info('- Getting demands...')
  database.get_demands()
  logger.info('- Mapping demands...')

  def print_demand(*args, **kwargs):
    pass

  database.map_demands(print_demand)
  logger.info('Testing Done.')

  logger.info('Main: Starting Mail Daemon...')
  mail_daemon = mail.daemon(
      config,
      gmail_credentials,
      database,
      ci_commit_sha,
  )
  logger.info('Main: Started Mail Daemon.')
  mode = config['main_config']['mode']
  if mode == 'loop':
    logger.info('Loop Mode:')
    loop(config, mail_daemon)
  else:
    logger.error(f'Main: Unknown mode "{mode}".')
    logger.error('Main: Exit w/ code 3.')
    exit(3)


if __name__ == '__main__':
  run()
