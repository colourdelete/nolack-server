set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "${RED}\"${last_command}\" command exited with exit code $?.${NC}"' EXIT

BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m' # May be brown sometimes!
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m' # It's nice to have both
LIGHTGREY='\033[0;37m'
DARKGRAY='\033[1;30m'
DARKGREY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
LIGHTORANGE='\033[1;33m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'
# Black        0;30     Dark Gray     1;30
# Red          0;31     Light Red     1;31
# Green        0;32     Light Green   1;32
# Brown/Orange 0;33     Yellow        1;33
# Blue         0;34     Light Blue    1;34
# Purple       0;35     Light Purple  1;35
# Cyan         0;36     Light Cyan    1;36
# Light Gray   0;37     White         1;37
# Thanks to <https://stackoverflow.com/a/5947802/12070265>!

echo "Nolack Server Installer Script"
echo "${RED}-${YELLOW}-${ORANGE}-${LIGHTGREEN}-${GREEN}-${LIGHTBLUE}-${BLUE}-${LIGHTPURPLE}-${PURPLE}-${NC}"
echo "Version <ci_commit_sha> (<ci_commit_tag>)"
echo "        <ci_commit_title>"
if [ "$EUID" -ne 0 ]
  then echo "${RED}Please run the installer as root.${NC}"
  exit
fi

sudo systemctl stop nolack_server.service || echo "${CYAN}Not error - nolack_server wasnt existing.${NC}"

mkdir -p "/opt/nolack/nolack-server/<ci_commit_sha>" || echo "${CYAN}Not error - folder run exists.${NC}"
cd "/opt/nolack/nolack-server/<ci_commit_sha>"

rm -r nolack-server || echo "${CYAN}Same version not found.${NC}" && echo "${ORANGE}Same version found. Removed recursively.${NC}"
echo "${CYAN}Cloning...${NC}"
if [[ $* != *--skipinstall* ]]; then
  echo "${CYAN}Install Git...${NC}"
  apt update
  apt install git -y
else
  echo "${CYAN}SKIPPED Install Git.${NC}"
fi
git clone 'https://gitlab.com/colourdelete/nolack-server.git'
echo "${CYAN}Cding & Updating...${NC}"
cd nolack-server
git checkout stable
git pull
echo "${CYAN}Copying systemd stuff...${NC}"
rm -r "/opt/nolack/nolack-server/run" || echo "${CYAN}Not error - folder run does not exist.${NC}"
mkdir -p "/opt/nolack/nolack-server/run"
cp "/opt/nolack/nolack-server/<ci_commit_sha>/nolack-server/installer_help/autoboot.sh" "/opt/nolack/nolack-server/run/autoboot.sh"
cp "/opt/nolack/nolack-server/<ci_commit_sha>/nolack-server/installer_help/update.sh" "/opt/nolack/nolack-server/run/update.sh"
cp "/opt/nolack/nolack-server/<ci_commit_sha>/nolack-server/installer_help/nolack_server.service" "/etc/systemd/system/nolack_server.service"

echo "${CYAN}Setting up systemd stuff... (mkdir, chown, chmod, daemon reload, enable)${NC}"
chown root:root "/opt/nolack/nolack-server/run/autoboot.sh"
chmod 755 "/opt/nolack/nolack-server/run/autoboot.sh"
chown root:root "/opt/nolack/nolack-server/run/update.sh"
chmod 755 "/opt/nolack/nolack-server/run/update.sh"
systemctl daemon-reload || echo "${ORANGE}Systemctl stuff MIGHT NOT WORK.${NC}"
systemctl enable nolack_server.service || echo "${ORANGE}Systemctl stuff MIGHT NOT WORK.${NC}"

echo "${CYAN}Setting up config...${NC}"
mkdir -p "/opt/nolack/nolack-server/config" || echo "${CYAN}Not error - folder config does not exist.${NC}"
cp "/opt/nolack/nolack-server/<ci_commit_sha>/nolack-server/config.yml" "/opt/nolack/nolack-server/config/config.yml"
echo ''
echo ''
echo ''
echo "${ORANGE}"
echo 'YOU NEED TO ADD YOUR OWN CREDENTIAL(S) (service-account.json).'
echo "${NC}"
echo ''
echo ''
echo ''

if [[ $* != *--skipinstall* ]]; then
  echo "${CYAN}Setup software...${NC}"
  apt update
  apt install python3 python3-pip -y
  python3 -m pip install virtualenv
  python3 -m pip install --upgrade virtualenv
else
  echo "${CYAN}SKIPPED Setup software.${NC}"
fi
echo "${CYAN}Sourcing venv to check...${NC}"
python3 -m virtualenv venv
source venv/bin/activate
python3 -m pip install --upgrade -r '/opt/nolack/nolack-server/<ci_commit_sha>/nolack-server/requirements.txt'
echo "${CYAN}Sourced venv to check.${NC}"
echo ''
# systemctl status nolack_server.service || echo "${CYAN}Not error - non-zero (${?}).${NC}"
echo ''
echo 'Start Nolack Server using:'
echo 'sudo systemctl start nolack_server.service'
echo ''
echo 'Stop Nolack Server using:'
echo 'sudo systemctl stop nolack_server.service'
echo ''
echo 'Thank you for installing Nolack Server!'
echo ''
echo "${LIGHTGREEN}Finished WITHOUT ERRORS! Exiting...${NC}"
exit
